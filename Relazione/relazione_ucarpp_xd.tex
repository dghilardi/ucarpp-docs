\documentclass[11pt,a4paper]{article}

\usepackage[latin1]{inputenc}
%\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[italian]{babel}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{array}
\usepackage{pgfplots}
\usepackage{tikz}
\usepackage{verbatim}
\usepackage[boxed]{algorithm2e}
%\usepackage{vmargin}
\usepackage{geometry}

\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{longtable}

\linespread{1.3}

\usepackage[section]{placeins}
\hyphenation{fea-si-ble}

\usepackage{caption}
\usepackage{subcaption}
\usepackage{rotating}
%\usepackage[margin=1.0in]{geometry}
\renewcommand{\thesubfigure}{\arabic{subfigure}}
%\renewcommand{\figurename}{Fig.}

\newcommand{\largemargins}{\newgeometry{left=0.5cm,right=0.5cm,top=0.5cm,bottom=0.5cm}}

\usepackage[colorlinks=true]{hyperref}
\hypersetup{
	bookmarksnumbered=true,
	linkcolor=black,
	citecolor=black,
	pagecolor=black,
	urlcolor=black,
}

\begin{document}

\title{UCARPP, una meta-euristica}
\author{Mirko Bellini (91183)\\
	    Davide Ghilardi (91641)\\
		Ilaria Martinelli (90870)\\
		Enrico Piantoni (91220)}
\date{\today}

\maketitle

\begin{center}
	Elaborato di \\
	Algoritmi di ottimizzazione
\end{center}


\pagestyle{plain}
%\frontmatter
\tableofcontents

%\mainmatter
\section{Il problema}
� dato un grafo indiretto $\Gamma = (V, E)$, con $V$ insieme dei vertici del grafo e $E$ insieme dei lati del grafo. 

Nel vertice 0, che identifica il deposito, sono situati $m$ veicoli identici con stessa capacit� $Q$ e stesso tempo limite $T_{max}$ sulla durata del loro percorso.

Ad ogni lato $e \in E$ � associato un tempo di percorrenza $t_e$ che rispetta la disuguaglianza triangolare. Inoltre esite un sottoinsieme di lati $L \subset E$, detti profittabili, che hanno un profitto positivo $p_e$ e una domanda $d_e$ oltre al tempo di percorrenza $t_e$.
Il profitto di un lato pu� essere raccolto solo una volta e il veicolo che lo raccoglie deve servire la domanda corrispondente.

Il problema noto come UCARPP (Undirected Capacitated Arc Routing Problem with Profits) cerca un set di percorsi, uno per ogni veicolo, che massimizzi il profitto totale. 
Inoltre ogni percorso deve partire e arrivare al deposito e la sua durata deve essere inferiore al tempo massimo $T_{max}$. 
Infine la quantit� totale che ogni veicolo raccoglie deve essere inferiore alla sua capacit� massima $Q$.

\section{La modellizzazione}
La modellizzazione del problema in esame, noto come UCARPP � stata realizzata a partire dalla modellizzazione di Belenguer e Benavent del problema CARP, (Sparse Formulation 1990-1998)\cite{art:BandB}.

Definito $P={1,\dots,m}$ l'insieme dei percorsi, uno per ogni veicolo, introduciamo delle variabili denotate come $x_{p,e}$ e $y_{p,e}$ (per $p \in P$ e $e \in E$). Le prime, $x_{p,e}$, saranno variabili intere e binarie e il loro significato sar�:
\[ 
x_{p,e} =
\begin{cases}
	1& \mbox{se il veicolo $p$ raccoglie il profitto del lato $e$} \\
	0& \mbox{altrimenti}\\
\end{cases}
\]
Le seconde, $y_{p,e}$, sono di tipo intero positivo (0 incluso) e indicano quante volte il veicolo $p$ passa sul lato $e$.

Da queste definizioni sorge spontaneamente il vincolo:
\[x_{p,e}\leq y_{p,e}\]
per ogni lato $e\in L$ ed ogni veicolo $p\in P$, in quanto � possibile raccogliere il profitto di un lato solo se ci si passa.

Poich� il profitto del lato $e\in L$ pu� essere raccolto da un solo veicolo $p\in P$, si ha che:
\[\sum_{p=1}^{m}x_{p,e}\leq1\]
in quanto si avr� $x_{p,e} = 1$ al pi� per un solo valore di $p$.

Per quanto riguarda il vincolo sulla capacit� dei veicoli, questo pu� essere espresso come:
\[\sum_{e\in L}d_{e}x_{p,e}\leq Q\]
infatti per ogni veicolo $p\in P$, la somma delle domande dei lati il cui profitto viene raccolto deve essere inferiore alla capacit� $Q$ del veicolo.

Per rappresentare il vincolo temporale sulla durata dei percorsi, si aggiunge il vincolo:
\[\sum_{e\in E} t_{e}y_{p,e} \leq T_{max}\]
che indica, per ogni $p\in P$, che la durata totale del percorso del veicolo $p$ deve essere in inferiore al tempo limite $T_{max}$.

Infine � necessario aggiungere i vincoli di connessione e di parit�. 
Quindi, per ogni insieme di vertici $S\subseteq V\setminus\left\{ 0\right\}$ si definiscono gli insiemi:
\[ \delta(S)= \lbrace(i,j)\in E \mid i \in S, j\in V\setminus S \rbrace \]
\[ E_L(S)= \lbrace(i,j)\in L \mid i,j \in S\rbrace\]
e per ogni $S$, $p\in P$ e $k\in E_{L}\left(S\right)$ si aggiungono i vincoli:
\[ \sum_{e\in\delta(S)}y_{p,e}\geq 2 x_{p,k}  \]
e
\[ \sum_{e\in\delta(S)}y_{p,e}\equiv0 \mod2 \]
Il primo (vincolo di connessione) esprime il fatto che se il veicolo $p$ serve un lato, allora il suo percorso deve connettere questo lato al deposito (vertice 0).
Il secondo (vincolo di parit�) specifica che ogni percorso induce un grafo pari (in cui i vertici hanno grado pari).

Il vincolo di parit�, cos� espresso, non � lineare. Una sua linearizzazione, seguendo quanto fatto da Belenguer e Benavent \cite{art:BandB}, �:
\[\sum_{e\in\delta(S)}y_{p,e} \geq 2 \sum_{e\in F} x_{p,e}-|F|+1  \]
per ogni $F\subseteq\delta(S)$ con cardinalit� $|F|$ dispari.

Di seguito � riportata la modellizzazione completa:
\begin{equation*}
\max\sum_{p=1}^{m}\sum_{e\in L}p_{e}x_{p,e}\\
\end{equation*}
soggetto a:
\begin{flalign}
&\sum_{p=1}^{m}x_{p,e}\leq1  && e\in L\label{unavolta} \\
&\sum_{e\in L}d_{e}x_{p,e}\leq Q  && p\in P \label{capacita} \\
&\sum_{e\in E} t_{e}y_{p,e} \leq T_{max}  && p\in P \label{tempomax} \\
&\sum_{e\in\delta(S)}y_{p,e}\equiv0 \mod2  && S\subseteq V\setminus\left\{ 0\right\}; && p\in P \label{entraesci} \\
&\sum_{e\in\delta(S)}y_{p,e}\geq 2 x_{p,
k}  && S\subseteq V\setminus\left\{ 0\right\}; && p\in P; && k\in E_{L}\left(S\right) \label{entraprendi} \\
&x_{p,e}\in\left\{ 0,1\right\}  && e\in L; &&p\in P \label{x} \\
&y_{p,e}\in\mathbb{Z}_{0}^{+}  && e\in E; && p\in P \label{y} \\
&x_{p,e}\leq y_{p,e} && e\in L; &&p\in P \label{legame}
\end{flalign}

\section{Un algoritmo risolutivo}
Per trovare una soluzione al problema, era richiesto di introdurre e implementare una meta-euristica.

Il gruppo ha deciso di orientarsi verso un approccio simile alla TABU search e di non ottimizzare eccessivamente l'algoritmo costruttivo.

Le istanze fornite propongono per ogni grafo solo i lati appartenenti a $L$. 
Nonostante fosse possible integrare questa informazione, rendendo il grafo completamente magliato, sfruttando la disuguaglianza triangolare, � stato deciso di non farlo principalmente per due motivi:
\begin{itemize}
	\item i lati introdotti non porterebbero alcun profitto potenziale
	\item non vi � limite al numero di volte che un veicolo percorre un lato
\end{itemize}

Il nostro obiettivo sar� creare percorsi i cui lati siano completamente profittabili, minimizzando, in una seconda fase volta all'ottimizzazione, la sovrapposizione di pi� percorsi.

%%FIGURA

\subsection{Algoritmo costruttivo}
Lo scopo dell'algoritmo costruttivo (Alg. \ref{alg:costruttiva}) � fornire una soluzione di partenza feasible che possa poi essere migliorata nella fase successiva.

\begin{algorithm}[H]
% \SetAlgoLined
 \KwData{Grafo $\Gamma$, set di veicoli P}
 \KwResult{Un set di percorsi ammissibili $route[1\dots LENGTH(P)]$}
 L $\leftarrow $list of all $e \in L$\;
 SORT(L)\;
 $route[1\dots LENGTH(P)]=\emptyset$\;
 \For{$p \in P$}{
 \For{$l \in L$}{
 	\eIf{IS-FEASIBLE(CONNECT(route[p], l))}{
 	route[p] $\leftarrow$ CONNECT(route[p], l)\;
	rimuovi l da L\;
	}{
	BREAK;
	}
 }
 }
 \caption{Fase costruttiva}
 \label{alg:costruttiva}
\end{algorithm}

L'obiettivo � costruire cicli chiusi sul deposito (nodo \{0\}), che rispettino il vincolo sul tempo (\ref{tempomax}), aggiungendo lati al ciclo finch� non viene violato il vincolo sulla capacit� (\ref{capacita}), per poi prendere l'ultima soluzione feasible che ne rappresenter� la soluzione migliore.

Viene creata la lista $L$ da cui scegliere i lati di cui si vuol raccogliere il profitto.
I lati sono inseriti dopo averli ordinati in base a profitto decrescente.
A parit� di profitto, i lati sono ordinati in base a costo crescente e a parit� di costo, in base a domanda crescente.

Come primo passo l'algoritmo selezioner� il primo lato della lista $L$ ed utilizzando l'albero di costo minimo, prodotto con l'algoritmo di Dijkstra, costruir� un ciclo chiuso tra il lato e il deposito.
Nel caso di ciclo feasible, si proceder� con l'aggiunta del secondo lato. Per far questo si cercher� un ciclo chiuso, sfruttando le informazioni dell'albero minimo, che contenga entrambi i lati scelti.
La procedura continua fino a che non verr� violato il vincolo sul tempo o sulla capacit�. A questo punto la soluzione designata, per l'attuale veicolo, sar� l'ultima feasible.

L'algoritmo procede poi con il calcolo dei percorsi per gli altri veicoli.

\subsubsection{Costruzione dei percorsi}
Un percorso viene composto da CONNECT(route[p], l) mediante i \emph{lati selezionati}\footnote{Lati la cui inclusione nel percorso � dettata dalle loro posizione nella lista $L$.} $L_s$ ed i lati necessari per congiungerli. La scelta dei \emph{lati selezionati} viene effettuata selezionando la testa della lista $L$ e rimuovendo l'elemento.

Per ogni passo dell'algoritmo, una volta definito l'insieme $L_s$, si ricerca, tramite l'albero dei costi minimi, l'insieme dei lati congiungenti a costo minore.

\subsubsection{Miglioramento fase costruttiva}
Al termine della fase costruttiva si pu� notare che spesso i percorsi creati ripassano pi� volte su alcuni lati. Questo � principalmente dovuto alla scelta di utilizzare l'albero dei costi minimi. 

Allo scopo di migliorare la soluzione trovata, viene effettuata un'analisi al fine di individuare questi \emph{lati svantaggiosi}\footnote{Sono definiti come i lati con pi� di un passaggio, considerando tutte le route} e si cercher� quindi di evitare il passaggio per questi lati formando delle deviazioni partendo dai lati adiacenti ai nodi del lato svantaggioso. 

Se queste deviazioni esistono, cio� rispettano il vincolo di tempo (\ref{tempomax}), non modificano i lati selezionati della route. La nuova soluzione avr� quindi lo stesso profitto, ma sar� un punto di partenza migliore per la prossima fase.

\subsubsection{Ottimizzazione della fase costruttiva}
Inizialmente � stato adottato un approccio \emph{seriale} (Alg. \ref{alg:costruttiva}), tramite la formazione sequenziale dei percorsi.
In questo modo il primo percorso sar� composto dai lati migliori, andandoli a \emph{rubare} agli altri percorsi. La soluzione trovata sar� molto buona per i primi percorsi, mentre gli ultimi saranno svantaggiati. 

Per evitare una distribuzione cos� sbilanciata � stato deciso di mediare la scelta dei lati tra tutti i percorsi, assegnando a turno un lato della lista $L$ a ciascun ciclo, adottando quindi un approccio \emph{parallelo} (Alg. \ref{alg:ottcostruttiva}).\\

\begin{algorithm}[H]
% \SetAlgoLined
 \KwData{Grafo $\Gamma$, set di veicoli P}
 \KwResult{Un set di percorsi ammissibili $route[1\dots LENGTH(P)]$}
 L $\leftarrow $list of all $e \in L$\;
 SORT(L)\;
 $route[1\dots LENGTH(P)]=\emptyset$\;
 selectedRoute $\leftarrow$ route[1]\;
    \Repeat{tutti i lati l sono stati considerati}{
	\Repeat{IS-FEASIBLE(selectedRoute) OR \\
	$\forall$ route r $\nexists$ IS-FEASIBLE(CONNECT(l,r))}{
		\If{IS-FEASIBLE(CONNECT(l,selectedRoute)}{
			route[p] $\leftarrow$ CONNECT(route[p], l)\;
		}
		selectedRoute $\leftarrow $ NEXT(selectedRoute)\;
    }
}
 \caption{Ottimizzazione fase costruttiva}
 \label{alg:ottcostruttiva}
\end{algorithm}

\subsubsection{Analisi prestazioni}
Analizzando alcune esecuzioni dell'algoritmo, si pu� notare come si creino multipli passaggi sullo stesso lato che portano la soluzione di partenza ad essere piuttosto scadente.

Questo risultato pu� essere dovuto al fatto che l'algoritmo utilizza come informazioni sui lati il solo percorso a costo minimo senza tener traccia in nessun modo dei lati gi� visitati dal veicolo in esame e da tutti gli altri. Da questo si evince che i lati maggiormente selezionati saranno quelli pi� vicini al nodo rappresentante il deposito, con costo minore.



\subsection{Algoritmo migliorativo}
La seconda fase riguarda l'applicazione di meta-euristiche per migliorare la soluzione di partenza. La meta-euristica prescelta � la TABU, o memory adaptive.

La TABU search � stata implementata mediante due funzioni, tabuStep() (Alg. \ref{alg:tabusearch}) che rappresenta la mossa della tabu e tabuMacroStep() (Alg. \ref{alg:macrotabusearch}) che implementa la mossa e gestisce le tabu list.

Di seguito vengono introdotti i concetti che hanno portato a questa implementazione.



\begin{algorithm}[H]
% \SetAlgoLined
 \KwData{Grafo $\Gamma$, set di percorsi ammissibili $route[1\dots LENGTH(P)]$, numero massimo di lati da rimuovere $N_{rem}$, lunghezza dell'intervallo di pulizia della route $cleanInterval$}
 \KwResult{}
 
 $tabuStop[1\dots LENGTH(P)] \leftarrow false$\;
 step $\leftarrow$ 0\;
 MAX\_PROFIT $\leftarrow$ 0\;
 positiveTrend $\leftarrow$ 0\;
 \Repeat{!IS\_ALL\_FALSE(tabuStop)}{
	\For{$p \in [1\dots LENGTH(P)]$}{
		tabuStop[p]$\leftarrow$TABU\_MACRO\_STEP($\Gamma$, $route[p]$, $N_{rem}$)\;
		\If{(step$\% cleanInterval$)==0}{
			CLEAN($route[p]$)\;
		}
		COLLECT\_ROUTES($route$)\;
		\If{PROFIT($route$) > MAX\_PROFIT}{
			MAX\_PROFIT $\leftarrow$ PROFIT(route)\;
			ST\_BACKUP(route)\;
		}
		positiveTrend $\leftarrow$ GET\_ANDAMENTO($route[p]$)\;
		\eIf{positiveTrend}{
			DECREASE\_LENGTH($tabu_{in}$)\;
			DECREASE\_LENGTH($tabu_{out}$)\;
		}{
			INCREASE\_LENGTH($tabu_{in}$)\;
			INCREASE\_LENGTH($tabu_{out}$)\;
		}
	}
	$step++$\;	
 }
 ST\_RESTORE(route)\;
 
 \caption{Gestione della Tabu search (tabuSearch)}
 \label{alg:tabusearchhandle}
\end{algorithm}

Le funzioni implementate risultano essere:
\begin{description}
	\item[CLEAN(route)] seleziona i soli lati raccolti dalla route e la ricostruisce.
	\item[COLLECT\_ROUTES(route\_list)] costituisce un passo migliorativo, in quanto prende in considerazione tutte le route nel processo di scelta dei lati da cui raccogliere. Procede selezionando tutti i lati $e$ da cui passa almeno una route, li riordina in base a: 
\[\frac{PROFIT(e)}{DEMAND(e)}-SIZE \{r| e \in r, e \in E, r\:route \},\]
e li segna come raccolti scorrendo le route in ordine.
	\item[GET\_ANDAMENTO(route)] ritorna un booleano che descrive se l'andamento attuale della tabu � positivo.
\end{description}


\begin{algorithm}[H]
% \SetAlgoLined
 \KwData{Grafo $\Gamma$, lista di Edge $Route$, numero massimo di lati da rimuovere $N_{rem}$}
 \KwResult{Valore booleano che indica se una ricerca tabu non � in fase di stallo}
 
 \Repeat{IS\_NOT\_NULL($m_{best}$)}{
 		$m_{best} \leftarrow $ MOSSA\_TABU($\Gamma$, Route, $N_{rem}$)\;
 	\eIf{IS\_NOT\_NULL($m_{best}$)}
 	{
 		DISCARD($m_{best}$.removedEdge)\;
 		BUILD\_ROUTE(Route, $m_{best}$)\;
		KNAPSACK\_GREEDY($\Gamma$, Route)\;
 		$tabu_{out}$.PUSH($m_{best}$.addedEdge) \;  
 		DECREASE($tabu_{in}$)\;
 		DECREASE($tabu_{out}$)\;
 	}
 	{
 		\tcp{Aspiration tabu}
		RELEASE\_FIRST\_EDGE($tabu_{in}$)\;
 		RELEASE\_FIRST\_EDGE($tabu_{out}$)\;
 		\If{EMPTY($tabu_{in}$) OR EMPTY($tabu_{out}$)}
 		{\Return false\;}
 	}
 }
 
 DECREASE($tabu_{in}$)\;
 DECREASE($tabu_{out}$)\;
 
 \Return true\;
 \caption{Macro mossa della Tabu search (tabuMacroStep)}
 \label{alg:macrotabusearch}
\end{algorithm}

La funzione implementata risulta essere:
\begin{description}
	\item[BUILD\_ROUTE(Route, m\_best)] costruisce la route applicando la mossa $m_{best}$ e aggiunge i lati rimossi alla $tabu_{in}$.
\end{description}

\begin{algorithm}[H]
% \SetAlgoLined
 \KwData{Grafo $\Gamma$, lista di Edge $Route$, numero massimo di lati da rimuovere $N_{rem}$}
 \KwResult{La mossa migliore $m_{best}$}
 \For{$N \in [1,N_{rem}]$}{
	 \For{$l \in Route$}{
		 \eIf{$\nexists l_N$}{ 
		 \tcp{$l_N$ sono N lati consecutivi a partire da l}
		 \Return $NULL$\;}{
			 \If{Nessuno dei lati di $l_N$ � tabu}{
				 $r_{residua} \leftarrow$ REMOVE($l_N$,Route)\;
				 \For{$l_e \in EDGE(\Gamma)$}{
				 	$r_{newPath} \leftarrow $BUILD\_PATH($r_{residua}$, $l_e$)\;
					\If{PROFIT($r_{newPath}$)>PROFIT($r_{best}$)}{
						$m_{best} \leftarrow$ mossa che ha costruito $r_{newPath}$
					}
				 }
			 }
		 }
		}
	 }
 \Return $m_{best}$\;
 \caption{Mossa della Tabu search (tabuStep)}
 \label{alg:tabusearch}
\end{algorithm}

Le funzioni implementate risultano essere:
\begin{description}
	\item[BUILD\_PATH(r\_residua, l\_e)] costruisce nuovo percorso che conterr� solamente il lato $l_e$ e i lati necessari per connetterlo a $r_{residua}$.
	\item[PROFIT(r\_newPath)] calcola il profitto utilizzando l'algoritmo knapsack greedy.
\end{description}


\subsubsection{Mossa}
Per l'applicazione della TABU � necessario definire un concetto di mossa. Chiamiamo mossa la rimozione di un numero $N$ di lati, $(x_1,x_2,\dots,x_N)$, da sostituire con un lato appartenente all'intorno $I$ e i lati necessari a richiudere il ciclo con un costo minimo. 

L'intorno $I$ era inizialmente stato ridotto a tutti e soli i lati che abbiano un vertice coincidente con il vertice in comune ai lati $x_0$ e $x_1$. Tuttavia questa soluzione, che richiedeva di calcolare i lati adiacenti, risultava troppo riduttiva.
L'intorno � quindi stato esteso a tutti i lati liberi.

Nella figura \ref{fig:mossa} � illustrata la sostituzione di $N=2$ lati.

\begin{figure}[ht]
	\centering
		\includegraphics[width=10cm]{img/mossa}
	\caption{esempio di mossa con sostituzione di $N=2$ lati}
	\label{fig:mossa}
\end{figure}

La ricostruzione del ciclo fatta dalla funzione BUILD\_PATH() dopo aver tolto gli $N$ lati � fatta congiungendo il lato prescelto con il lato $x_0$ e il lato $x_{N+1}$ utilizzando l'albero dei cammini minimi. 

Inizialmente, se il percorso cos� ottenuto conteneva anche un solo lato TABU, la soluzione veniva scartata. 
Tuttavia in un secondo momento � stato deciso di evitare di scartare la mossa intera, ma di cercare vie alternative per ricollegare il lato prescelto al ciclo in esame. Questo viene fatto cercando per un numero fisso di tentativi, RECONSTRUCTION\_TOLERANCE=7, una strada che colleghi il lato prescelto saltando il lato TABU utilizzando un lato a questo adiacente.

\subsubsection{Activation rule}
Per quanto riguarda le regole di attivazione della tabu � stato deciso di distinguere la rimozione rispetto all'aggiunta di un arco. 

La tabu tenure degli archi rimossi � stata impostata pi� alta rispetto a quella per i lati inseriti, per favorire l'esplorazione dello spazio delle soluzioni.

Sono quindi state implementate tabu list ingresso ed uscita per ogni percorso. Sono state implementate tabu tenure dinamiche, pi� lunghe nel caso in cui la soluzione stia peggiorando, pi� corte altrimenti.

\subsubsection{Criteri di aspirazione}
Per quanto riguarda i criteri di aspirazione � stato implementato il criterio \emph{default}. Nel caso in cui non siano disponibili mosse viene decrementata la tabu tenure rimanente fino a che non viene liberata una mossa (RELEASE\_FIRST\_EDGE()).

\subsubsection{Raccolta dei profitti}
I cicli vengono costruiti, o meglio modificati, tenendo in considerazione solo il vincolo sul tempo (\ref{tempomax}). Per decidere di quali lati raccogliere il profitto, � stato implementato un algoritmo greedy (Alg. \ref{alg:knapsackGreedy}) per risolvere il knapsack problem considerando i profitti $p_i$ e le domande $d_i$ come pesi.

Quindi tutti i lati del ciclo in esame vengono ordinati per rapporto $p_i/d_i$ decrescente e vengono raccolti i profitti di tutti i lati liberi (non ancora raccolti) fintanto che viene rispettato il vincolo sulla capacit� (\ref{capacita}).


\begin{algorithm}[H]
 \KwData{Grafo $\Gamma$, route Route}
 \KwResult{Route con dei lati raccolti}
 workingList $\leftarrow$ lati distinti di Route\;
 SORT(workingList) \tcp{ordina per profitto su domanda}
 caricoTotale $\leftarrow$ 0\;
 \For{$l \in workingList$}{
 \If{l non � stato raccolto || � stato gi� raccolto da Route}
 {
 \eIf{caricoTotale+DOMANDA(l)<=CAPACIT�\_VEICOLO($\Gamma$)}
 {
 COLLECT(l)\;
 caricoTotale $\leftarrow$ caricoTotale+DOMANDA(l)\;
 }{
 DISCARD(l);
 }
 }
 }
 \caption{Knapsack greedy}
 \label{alg:knapsackGreedy}
\end{algorithm}

\subsubsection{Memoria adattiva}
La memoria adattiva si compone di due fasi: \emph{short term memory} e \emph{long term memory}. L'applicazione delle regole della tabu avviene durante la short term memory, in cui vengono raccolte informazioni. Le informazioni raccolte sono necessarie per la seconda fase, in cui si decide che politica applicare sulla short term. 
Una volta che la tabu-search giunge al termine, la ricerca viene rilanciata favorendo l'ingresso dei lati meno utilizzati nelle precedenti esecuzioni (differenziazione).

\subsubsection{Esecuzione}
L'algoritmo procede eseguendo una mossa per ogni ciclo. 
Se il profitto dei cicli non migliora per un numero $TABU\_LIMIT$ di volte, allora la fase di \emph{short term memory} termina e si procede con un rilancio a partire da una nuova soluzione di partenza. La nuova soluzione � creata sfruttando l'algoritmo costruttivo, ma ordinando i lati principalmente per frequenza in modo da avvantaggiare i lati che non sono mai stati usati nella soluzione precedente, per esplorare altre soluzioni.

Questo procedimento viene effettuato $NUM\_RILANCI$ volte fintanto che si ottiene un miglioramento. Dopo  $NUM\_RILANCI$ volte senza miglioramenti l'algoritmo si ferma.



\include{implementazione}
\include{conclusioni}

\appendix
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%THE END
\section{Un esempio di esecuzione}
Di seguito si mostra la risoluzione di un'istanza: si � presa in considerazione l'istanza \emph{val4D} con 2 veicoli con i dati standard. Per brevit� si � deciso di rappresentare solo il secondo rilancio dell'algoritmo.

\subsection{Rappresentazione}
Nel rappresentare i percorsi sul grafo � stata utilizzata la seguente notazione: differenti colori indicano differenti veicoli, la freccia sar� tratteggiata nel caso in cui il lato sia di passaggio, continua nel caso in cui si raccolga da tale lato. Nel caso in cui un veicolo passi pi� volte su un lato da cui si raccoglie, tutte le freccie saranno continue in modo da lasciare libert� riguardo a quando raccogliere.
%\include{esempioEsecuzione}

\subsection{Descrizione istanza}
Di seguito � riportato il file dell'istanza considerata.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth]{img/val4D}
	\caption{Grafo}
	\label{fig:g}
\end{figure}

%\largemargins
\include{cost-4d2v}
%\restoregeometry

%\largemargins
\include{tabu-4d2v}
%\restoregeometry

%\section{Bibliografia}
  \nocite{*}
  \bibliographystyle{plain}
  \bibliography{bibliografia}
  
\end{document}
