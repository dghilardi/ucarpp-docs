#!/bin/bash

count=0;
count2=0;
for i in `seq 1 65`
do
  for j in `seq 0 1`
  do
    if [[ count -eq 0 ]]
    then
	echo "\end{sidewaysfigure}"
	echo "\clearpage"
	echo "\begin{sidewaysfigure}"
    fi
    count=$[(count+1)%6]
    count2=$[(count2+1)]

    filename="img/val4d2v-iter-6/tabu/iteration-${i}_veicolo-${j}";
    
    echo -e "\t\\\begin{subfigure}[b]{0.3\\\textheight}"
    echo -e "\t\t\\\includegraphics[width=\\\textwidth]{$filename}"
    echo -e "\t\t\\\caption{Migliorativo step $i, veicolo "$[j+1]"}"
    echo -e "\t\t\\\label{fig:m$count2}"
    echo -e "\t\\\end{subfigure}"
  done
done