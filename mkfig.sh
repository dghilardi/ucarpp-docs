#!/bin/bash

function printFig {
  echo -e '\t\\begin{subfigure}[b]{0.45\\textwidth}'
  echo -e "\t\t\\\includegraphics[width=\\\textwidth]{img/val4d2v-iter-6/costructive/fig-$1}"
  echo -e "\t\t\\\caption{Costruttivo fig $1}"
  echo -e "\t\t\\\label{fig:c$1}"
  echo -e "\t\\\end{subfigure}"
}

echo "\begin{figure}"
for i in `seq $1 $2`
do
  printFig $i
done
echo "\end{figure}"